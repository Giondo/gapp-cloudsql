provider "google" {
  # credentials = "${file("account.json")}"
  project     = var.PROJECT
  region      = var.REGION
}


resource "google_project_service" "servicenetworking" {
  project = var.PROJECT
  service =  "servicenetworking.googleapis.com"

  disable_dependent_services = true
}

resource "google_project_service" "vpcaccess" {
  project = var.PROJECT
  service =  "vpcaccess.googleapis.com"

  disable_dependent_services = true
}

resource "google_project_service" "appengine" {
  project = var.PROJECT
  service =  "appengine.googleapis.com"

  disable_dependent_services = true
}

resource "google_project_service" "computeapi" {
  project = var.PROJECT
  service =  "compute.googleapis.com"

  disable_dependent_services = true
}
