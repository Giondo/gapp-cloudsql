resource "google_sql_database" "database" {
  name     = "surety-db"
  instance = google_sql_database_instance.master.name
}
