resource "google_compute_global_address" "private_ip_address" {

  name          = "private-ip-address"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = google_compute_network.suretynet.self_link
}

resource "google_service_networking_connection" "private_vpc_connection" {

  network                 = google_compute_network.suretynet.self_link
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]

  depends_on = [google_project_service.servicenetworking]
}

resource "google_compute_network" "suretynet" {
  name = "surety-network"
}

resource "google_vpc_access_connector" "connector-appengine-db" {
  name          = "gapp-db"
  region      = var.REGION
  ip_cidr_range = "10.8.0.0/28"
  network       = google_compute_network.suretynet.name

  depends_on = [google_project_service.vpcaccess]
}
