output "PrivateIP" {
	value = google_sql_database_instance.master.ip_address.0.ip_address
}

output "Conection" {
	value = google_sql_database_instance.master.connection_name
}

output "vpc_access_connector" {
	value = google_vpc_access_connector.connector-appengine-db.id
}

output "DB_USER" {
	value = google_sql_user.useradmin.name
}

output "DB_PASS" {
	value = google_sql_user.useradmin.password
}

output "DB_NAME" {
	value = google_sql_database.database.name
}
