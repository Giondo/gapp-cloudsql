# resource "google_storage_bucket" "tf-state" {
#   name          = "${var.PROJECT}-terraform-states"
#   location      = var.REGION
#   force_destroy = true
#   storage_class = "STANDARD"
# }

resource "google_storage_bucket" "sql-import" {
  name          = "${var.PROJECT}-sqlimport"
  location      = var.REGION
  force_destroy = true
  storage_class = "STANDARD"
}
