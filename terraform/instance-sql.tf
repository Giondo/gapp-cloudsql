resource "google_sql_database_instance" "master" {
  name             = "mysql-surety"
  database_version = "MYSQL_5_7"
  region           = var.REGION

  deletion_protection  = "true"

  depends_on = [google_service_networking_connection.private_vpc_connection]

  settings {
    # Second-generation instance tiers are based on the machine
    # type. See argument reference below.
    tier = var.DBINSTANCETYPE
    availability_type = "REGIONAL"
    replication_type = "SYNCHRONOUS"

    backup_configuration {
      enabled    = true
      start_time = "23:00"
      binary_log_enabled = true
    }

    ip_configuration {
        ipv4_enabled    = false
        private_network = google_compute_network.suretynet.self_link
    }
  }
}
locals {
  db_instance_creation_delay_factor_seconds = 60
}
