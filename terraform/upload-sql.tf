resource "google_storage_bucket_object" "sqlimport" {
  name   = "sqlimport"
  source = "../data/fromaccess.sql"
  bucket = google_storage_bucket.sql-import.name

  depends_on = [google_storage_bucket.sql-import]
}
