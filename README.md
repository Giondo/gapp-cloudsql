# Connecting to Cloud SQL - MySQL

## Basic auth

### Clone this repo

```
git clone https://gitlab.com/Giondo/gapp-cloudsql.git
```

### Need to authenticate with Google SDK

```bash
gcloud auth application-default login
gcloud auth login
```
## Complete variables for Terraform

Please check the file "terraform/vars.tf"
Complete it with all your locations settings


## Running Terraform

This will create:

* All the Networking needed
* Cloud SQL instance
* vpc connector to access the DB from App Engine
* Bucket for upload sql database
* upload sql file to bucket so you can import the database on the instance

```bash
cd terraform
terraform init
terraform plan
terraform apply

```
## Complete App Engine variables

Please check the file "gapp/app.standard.yaml" and complete all the variables
with Terraform's output

Example:
```
Outputs:

Conection = playground-s-11-b1303b8a:us-east1:mysql-surety
DB_NAME = surety-db
DB_PASS = changeme
DB_USER = admin
PrivateIP = 10.116.0.2
vpc_access_connector = projects/playground-s-11-b1303b8a/locations/us-east1/connectors/gapp-db
```

## Deploying App Engine

```bash
cd gapp
gcloud app deploy app.standard.yaml

```

## Gitlab Pipeline

In order to make the gitlab pipeline work you will need to add two variables to your project

* SERVICE_ACCOUNT --> the content of the json file you created
* PROJECT_ID --> Google Project ID

Please be aware your iam permissions for this service account will need to have these permissions:

* App Engine Deployer
* App Engine Service Admin
* Cloud Build Service Account
* Compute Viewer
* Storage Object Creator
* Storage Object Viewer
* Serverless VPC Access User

# Password generator

```
date +%s | sha256sum | base64 | head -c 32 ; echo
```
